package ru.telegram.bots.core.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.telegram.bots.core.auth.entity.UserEntity;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@RepositoryRestResource(path = "users")
public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {
    @Transactional
    Optional<UserEntity> findByUsername(String username);
}
