package ru.telegram.bots.core.auth.enums;

public enum Role {
    USER, ADMIN
}
