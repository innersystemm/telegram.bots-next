package ru.telegram.bots.core.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class News {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fetchDate;

    @Column(length = 1000, nullable = false)
    private String content;
}
