package ru.telegram.bots.core.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.telegram.bots.core.auth.bean.AuthenticationResponse;
import ru.telegram.bots.core.auth.bean.Credentials;
import ru.telegram.bots.core.auth.configuration.JwtTokenProvider;
import ru.telegram.bots.core.auth.entity.UserEntity;
import ru.telegram.bots.core.auth.enums.Role;
import ru.telegram.bots.core.auth.exception.UserAlreadyExistsException;
import ru.telegram.bots.core.auth.repository.UserEntityRepository;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Value("${app.jwt.tokenType}")
    private String tokenType;

    @PostMapping("/login")
    public ResponseEntity <AuthenticationResponse> login(@Valid @RequestBody Credentials credentials) {
        val authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        credentials.getUsername(),
                        credentials.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        val token = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new AuthenticationResponse(token, tokenType));
    }

    @PostMapping("/register")
    public ResponseEntity<Boolean> register(@Valid @RequestBody Credentials credentials) {
        if (userEntityRepository.findByUsername(credentials.getUsername()).isPresent()) {
            throw new UserAlreadyExistsException("Username is already taken");
        }

        val user = UserEntity.builder()
                .active(true)
                .authorities(Collections.singletonList(Role.USER))
                .username(credentials.getUsername())
                .password(passwordEncoder.encode(credentials.getPassword()))
                .build();

        userEntityRepository.save(user);

        final URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(user.getUsername()).toUri();

        return ResponseEntity.created(location).body(true);
    }

    @PostMapping("/logout")
    public ResponseEntity<Boolean> logout() {
        SecurityContextHolder.clearContext();
        return ResponseEntity.ok(true);
    }
}
