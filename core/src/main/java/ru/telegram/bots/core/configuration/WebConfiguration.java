package ru.telegram.bots.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/{spring:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("(/**|^/api/**){spring:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/{spring:\\w+}(/**|^/api/**){spring:?!(\\.js|\\.css)$}")
                .setViewName("forward:/");
    }
}