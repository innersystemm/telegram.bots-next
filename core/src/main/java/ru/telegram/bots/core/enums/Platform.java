package ru.telegram.bots.core.enums;

public enum Platform {
    XBOX,
    PC,
    PS,
    NONE
}
