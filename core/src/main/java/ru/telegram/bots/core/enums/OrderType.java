package ru.telegram.bots.core.enums;

import lombok.Getter;

@Getter
public enum OrderType {
    COIN,
    FRANCHISE
}
