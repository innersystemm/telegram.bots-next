package ru.telegram.bots.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.telegram.bots.core.domain.NewsletterSubscriber;

@Repository
@RepositoryRestResource
public interface NewsletterSubscriberRepository extends PagingAndSortingRepository<NewsletterSubscriber, Long>, CrudRepository<NewsletterSubscriber, Long> {
}
