package ru.telegram.bots.core.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class NewsCompilation {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private String compilationSubject;

    @ManyToMany
    private List<News> newsList;
}
