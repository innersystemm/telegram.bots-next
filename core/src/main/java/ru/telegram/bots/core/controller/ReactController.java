package ru.telegram.bots.core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@ControllerAdvice
public class ReactController {
    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @GetMapping("/get-auth")
    @ResponseBody
    public boolean isAuthenticated(Authentication authentication) {
        return authentication != null && authentication.isAuthenticated();
    }
}
