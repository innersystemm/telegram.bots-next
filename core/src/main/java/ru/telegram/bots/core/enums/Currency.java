package ru.telegram.bots.core.enums;

public enum Currency {
    RUB,
    USD,
    UAH
}
