package ru.telegram.bots.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Locale;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {
    public static final Locale LC_DEFAULT = new Locale("ru", "RU");
}
