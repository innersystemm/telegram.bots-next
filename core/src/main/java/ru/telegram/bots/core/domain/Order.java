package ru.telegram.bots.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ColumnDefault;
import ru.telegram.bots.core.enums.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer_order")
public class Order {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long coinsCount;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Platform platform = Platform.NONE;

    @Column
    private String comment;

    @Column
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column
    @Temporal(TemporalType.TIME)
    private Date time;

    @Column(nullable = false)
    private BigDecimal orderPrice;

    @Column
    @NotNull
    @ColumnDefault("'NONE'")
    private String telephone = StringUtils.EMPTY;

    @ManyToOne
    @NotNull
    private Customer customer;

    @Column
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.UNPAID;

    @Column
    @Enumerated(EnumType.ORDINAL)
    private OrderType orderType = OrderType.COIN;

    @Column
    @Enumerated(EnumType.ORDINAL)
    private Currency currencyType = Currency.RUB;

    @Column
    @Enumerated(EnumType.ORDINAL)
    private FranchiseType franchiseType;
}
