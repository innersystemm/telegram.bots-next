package ru.telegram.bots.core.auth.entity;

import lombok.*;
import ru.telegram.bots.core.auth.enums.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class UserEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotEmpty
    private String username;

    @Column
    @NotEmpty
    private String password;

    @Column
    private Boolean active;

    @Column
    @Email
    private String email;

    @Enumerated(EnumType.ORDINAL)
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    private Collection<Role> authorities;
}
