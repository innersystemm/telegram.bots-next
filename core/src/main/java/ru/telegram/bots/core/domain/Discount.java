package ru.telegram.bots.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ru.telegram.bots.core.enums.Platform;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
public class Discount {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    @NonNull
    private Integer fromCount;

    @Column
    @NotNull
    @NonNull
    private Integer toCount;

    @Column
    @NotNull
    @NonNull
    private double pricePer100k;

    @Column
    @NonNull
    private Platform platform;

    public boolean ruleMatches(final Long coins, final Platform platform) {
        return fromCount <= coins && coins < toCount && this.platform == platform;
    }
}
