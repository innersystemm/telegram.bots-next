package ru.telegram.bots.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.telegram.bots.core.domain.Customer;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
@RepositoryRestResource
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query("select c from Customer c where c.userId=:userId")
    Optional<Customer> findByTelegramId(@NotNull @Param("userId") Long userId);

    @Query("select c from Customer c where c.userName=:userName")
    Optional<Customer> findByUserName(@NotNull @Param("userName") String userName);

}
