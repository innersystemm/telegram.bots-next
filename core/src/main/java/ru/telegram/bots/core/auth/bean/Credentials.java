package ru.telegram.bots.core.auth.bean;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class Credentials {
    @NotEmpty
    private String username;
    @Min(6)
    @NotEmpty
    private String password;
}
