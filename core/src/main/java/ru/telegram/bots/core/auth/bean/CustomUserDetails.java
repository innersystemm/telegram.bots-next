package ru.telegram.bots.core.auth.bean;

import lombok.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import ru.telegram.bots.core.auth.entity.UserEntity;
import ru.telegram.bots.core.auth.enums.Role;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CustomUserDetails extends User {
    public CustomUserDetails(@NonNull String username, @NonNull String password, @NonNull Collection<Role> authorities) {
        super(username, password, toCommaSeparatedString(authorities));
    }

    public CustomUserDetails(@NonNull UserEntity userEntity){
        super(userEntity.getUsername(), userEntity.getPassword(), toCommaSeparatedString(userEntity.getAuthorities()));
    }

    private static List<GrantedAuthority> toCommaSeparatedString(Collection<Role> roles) {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(roles.stream().map(Enum::toString).collect(Collectors.joining(",")));
    }
}
