package ru.telegram.bots.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.telegram.bots.core.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String firstName;

    @Column
    private String secondName;

    @Column
    private String userName;

    @NotNull
    @Column(unique = true)
    private Long userId;

    @Column(unique = false)
    private String telephoneNumber;

    @Basic
    @Column
    @Builder.Default
    private Locale locale = Constants.LC_DEFAULT;
}
