package ru.telegram.bots.core.auth.service;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.telegram.bots.core.auth.bean.CustomUserDetails;
import ru.telegram.bots.core.auth.repository.UserEntityRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        val user = userEntityRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(String.format("User [%s] not found", username)));
        return new CustomUserDetails(user);
    }
}
