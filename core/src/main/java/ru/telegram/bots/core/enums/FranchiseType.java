package ru.telegram.bots.core.enums;

public enum FranchiseType {
    STANDART,
    PLUS,
    ELITE
}
