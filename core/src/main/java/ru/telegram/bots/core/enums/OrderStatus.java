package ru.telegram.bots.core.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum OrderStatus {
    UNPAID("Не оплачен"),
    PAID("Оплачен");
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
