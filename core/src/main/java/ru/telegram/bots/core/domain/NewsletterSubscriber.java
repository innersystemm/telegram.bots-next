package ru.telegram.bots.core.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class NewsletterSubscriber {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private Double balance;

    @Column
    private Boolean isActive;
}
