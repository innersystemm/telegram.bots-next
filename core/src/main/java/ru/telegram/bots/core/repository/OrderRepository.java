package ru.telegram.bots.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.telegram.bots.core.domain.Order;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
@RepositoryRestResource
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("select o from Order o join o.customer c where c.userId=:customerId")
    List<Order> findByCustomerId(@NotNull @Param("customerId") Long customerId);
}
