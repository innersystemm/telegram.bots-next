package ru.telegram.bots.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.telegram.bots.core.domain.News;

@Repository
@RepositoryRestResource
public interface NewsRepository extends PagingAndSortingRepository<News, Long>, CrudRepository<News, Long> {
}
