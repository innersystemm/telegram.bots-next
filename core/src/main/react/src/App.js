import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import {makeStyles} from '@material-ui/core';
import Footer from './components/page-components/Footer';
import LoginPage from './components/pages/Login';
import Background from './background.jpg';

const useStyles = makeStyles({
  root: {
    backgroundImage: `url(${Background})`,
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center left',
    backgroundRepeat: ' no-repeat',
    backgroundSize: 'cover',
    transition: 'all 0.3s ease'
  },
  wrapper: {
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column'
  }
});

function App() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <BrowserRouter>
          <Route exact path="/login" component={LoginPage} />
          {/* <Route exact path="/manage">
          <Page className={classes.root}>
            <Switch>
              <Route path="/news" component={NewsList} />
              <Route component={UsersList} />
            </Switch>
          </Page>
          </Route> */}
        </BrowserRouter >
        <Footer height={11} />
      </div>
    </div>
  );
}

export default App;
