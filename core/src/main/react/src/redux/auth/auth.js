export const AUTH_LOGIN = 'AUTH/LOGIN';
export const AUTH_LOGIN_SUCCESS = 'AUTH/LOGIN/SUCCESS';
export const AUTH_LOGIN_FAILURE = 'AUTH/LOGIN/FAILURE';

export const AUTH_REGISTER = 'AUTH/REGISTER';
export const AUTH_REGISTER_SUCCESS = 'AUTH/REGISTER/SUCCESS';
export const AUTH_REGISTER_FAILURE = 'AUTH/REGISTER/FAILURE';

export const AUTH_LOGOUT = 'AUTH/LOGOUT';
export const AUTH_LOGOUT_SUCCESS = 'AUTH/LOGOUT/SUCCESS';
export const AUTH_LOGOUT_FAILURE = 'AUTH/LOGOUT/FAILURE';

export const processLogin = payload => ({ type: AUTH_LOGIN, payload });
export const processRegister = payload => ({ type: AUTH_REGISTER, payload });
export const processLogout = payload => ({ type: AUTH_LOGOUT, payload });

const initialState = {
    token: null,
    tokenType: null,
    error: false,
    loading: false
};

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_LOGIN:
            return {
                ...state,
                loading: true
            }
        case AUTH_LOGIN_SUCCESS:
            return {
                ...state,
                token: action.payload.token,
                tokenType: action.payload.tokenType,
                loading: true
            }
        case AUTH_LOGIN_FAILURE:
            return {
                ...state,
                error: true,
                loading: true
            }
        default:
            return state
    }
};