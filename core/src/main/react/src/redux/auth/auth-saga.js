import {call, put, takeLatest} from 'redux-saga/effects';
import {
    AUTH_LOGIN_FAILURE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT,
    AUTH_LOGOUT_FAILURE,
    AUTH_LOGOUT_SUCCESS,
    AUTH_REGISTER,
    AUTH_REGISTER_FAILURE,
    AUTH_REGISTER_SUCCESS
} from './auth';
import AuthApi from '../../api/AuthApi';

export default function* authSage() {
    // yield takeLatest(AUTH_LOGIN, login);
    yield takeLatest(AUTH_REGISTER, register);
    yield takeLatest(AUTH_LOGOUT, logout);
};

function* login(action) {
    try {
        const data = yield call(AuthApi.login, action.payload);
        yield put({ type: AUTH_LOGIN_SUCCESS, data });
    } catch (e) {
        yield put({ type: AUTH_LOGIN_FAILURE, usersError: e.message });
    }
};

function* register(action) {
    try {
        const data = yield call(AuthApi.register, action.payload);
        yield put({ type: AUTH_REGISTER_SUCCESS, data });
    } catch (e) {
        yield put({ type: AUTH_REGISTER_FAILURE, usersError: e.message });
    }
};

function* logout(action) {
    try {
        const data = yield call(AuthApi.logout, action.payload);
        yield put({ type: AUTH_LOGOUT_SUCCESS, data });
    } catch (e) {
        yield put({ type: AUTH_LOGOUT_FAILURE, usersError: e.message });
    }
};