import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {rootReducer} from './rootReducer';

import usersSaga from './users/users-saga';
import newsSaga from './news/news-saga';

const sagaMiddleware = createSagaMiddleware()
export const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware),
)

sagaMiddleware.run(usersSaga);
sagaMiddleware.run(newsSaga);

