import {call, put, takeLatest} from 'redux-saga/effects';
import {LOAD_USERS, LOAD_USERS_FAILURE, LOAD_USERS_SUCCESS} from './users';
import UsersApi from '../../api/UsersApi';

export default function* usersSaga() {
    yield takeLatest(LOAD_USERS, fetchUsers);
}

function* fetchUsers(action) {
    try {
        const data = yield call(UsersApi.fetchUsers, action.payload);
        yield put({ type: LOAD_USERS_SUCCESS, data });
    } catch (e) {
        yield put({ type: LOAD_USERS_FAILURE, usersError: e.message });
    }
}