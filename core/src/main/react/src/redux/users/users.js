export const LOAD_USERS = "USERS-LIST/LOAD";
export const LOAD_USERS_SUCCESS = "USERS-LIST/LOAD/SUCCESS";
export const LOAD_USERS_FAILURE = "USERS-LIST/LOAD/FAILURE";

export const loadUsers = payload => ({ type: LOAD_USERS, payload });

const initialState = {
    users: [],
    usersTotal: 0,
    usersError: null
}

export const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_USERS_SUCCESS:
            return {
                ...state,
                users: action.data._embedded.customers,
                usersTotal: action.data.page.totalElements
            }
        case LOAD_USERS_FAILURE:
            return {
                ...state,
                users: [],
                usersError: action.error
            }
        default:
            return state
    }
}