import {call, put, takeLatest, takeLeading} from 'redux-saga/effects';
import {
    ADD_NEWS,
    ADD_NEWS_FAILURE,
    ADD_NEWS_SUCCESS,
    DELETE_NEWS,
    LOAD_NEWS,
    LOAD_NEWS_FAILURE,
    LOAD_NEWS_SUCCESS
} from './news';

import NewsApi from '../../api/NewsApi';

export default function* newsSaga() {
    yield takeLatest(LOAD_NEWS, fetchNews);
    yield takeLeading(ADD_NEWS, addNews);
    yield takeLeading(DELETE_NEWS, deleteNews);
}

function* fetchNews(action) {
    try {
        const data = yield call(NewsApi.fetchNews, action.payload);
        yield put({ type: LOAD_NEWS_SUCCESS, data });
    } catch (e) {
        yield put({ type: LOAD_NEWS_FAILURE, usersError: e.message });
    }
}

function* addNews(action) {
    try {
        const data = yield call(NewsApi.addNews, action.payload);
        yield put({ type: ADD_NEWS_SUCCESS, data });
    } catch (e) {
        yield put({ type: ADD_NEWS_FAILURE, usersError: e.message });
    }
}

function* deleteNews(action) {
    try {
        const data = yield call(NewsApi.deleteNews, action.payload);
        yield put({ type: ADD_NEWS_SUCCESS, data });
    } catch (e) {
        yield put({ type: ADD_NEWS_FAILURE, usersError: e.message });
    }
}