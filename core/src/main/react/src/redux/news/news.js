export const LOAD_NEWS = "NEWS-LIST/LOAD";
export const LOAD_NEWS_SUCCESS = "NEWS-LIST/LOAD/SUCCESS";
export const LOAD_NEWS_FAILURE = "NEWS-LIST/LOAD/FAILURE";

export const ADD_NEWS = "NEWS-LIST/ADD_NEWS";
export const ADD_NEWS_SUCCESS = "NEWS-LIST/ADD_NEWS_SUCCESS";
export const ADD_NEWS_FAILURE = "NEWS-LIST/ADD_NEWS_FAILURE";

export const DELETE_NEWS = "NEWS-LIST/DELETE_NEWS";
export const DELETE_NEWS_SUCCESS = "NEWS-LIST/DELETE_NEWS_SUCCESS";
export const DELETE_NEWS_FAILURE = "NEWS-LIST/DELETE_NEWS_FAILURE";

export const loadNews = payload => ({ type: LOAD_NEWS, payload });
export const addNews = news => ({ type: ADD_NEWS, news });
export const deleteNews = news => ({ type: DELETE_NEWS, news });

export const initialState = {
    news: [],
    newsTotal: 0,
    newsError: null
}

export const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_NEWS_SUCCESS:
            return {
                ...state,
                news: action.data._embedded.customers,
                newsTotal: action.data.page.totalElements
            }
        case LOAD_NEWS_FAILURE:
            return {
                ...state,
                news: [],
                newsTotal: action.error
            }
        case ADD_NEWS_SUCCESS:
            return {
                ...state,
                news: [...state.news, action.news]
            }
        case ADD_NEWS_FAILURE:
            return {
                ...state,
                newsError: action.error
            }
        case DELETE_NEWS_SUCCESS: {
            return {
                ...state,
                news: [...action.news]
            }
        }
        case DELETE_NEWS_FAILURE: {
            return {
                ...state,
                newsError: action.error
            }
        }
        default:
            return state
    }
}