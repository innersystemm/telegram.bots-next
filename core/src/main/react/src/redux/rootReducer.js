import {combineReducers} from 'redux';
import {usersReducer} from './users/users';
import {newsReducer} from './news/news';
import {authReducer} from './auth/auth';

export const rootReducer = combineReducers({
    usersReducer,
    newsReducer,
    authReducer
});