import React from 'react';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles({
    root: props => ({
        marginTop: props.margin,
        marginBottom: props.margin
    })
})

function Divider({ className, margin = '1em' }) {
    const classes = useStyles({ margin })
    return (
        <hr className={`${classes.root} ${className}`} />
    )
}

export default Divider;