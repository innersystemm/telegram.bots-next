import React from 'react';
import {Divider, Grid, Paper, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles({
    title: {
        textAlign: 'center',
        padding: '.5vh',
    },
    option: {
        padding: '1vh',
        paddingLeft: '1vw',
        paddingRight: '1vw',
        cursor: 'pointer',
    },
    divider: {
        marginLeft: '3vh',
        marginRight: '3vh'
    },
    optionCounter: {
        textAlign: 'right',
    }
})

function Statistics({ square, className, title = '', children }) {
    const classes = useStyles();

    return (
        <Paper square={square} className={`${className} ${classes.root}`}>
            <Typography variant="h6" className={classes.title}>{title}</Typography>
            <Grid container>
                {children}
            </Grid>
        </Paper>
    );
}

function Option({ name, value }) {
    const classes = useStyles();

    return (
        <>
            <Grid container alignItems="center" className={classes.option}>
                <Grid item xs={10}>
                    <Typography variant="subtitle2">{name}</Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="subtitle2" className={classes.optionCounter}>{value}</Typography>
                </Grid>
            </Grid>
            <Divider className={classes.divider}/>
        </>
    );
}

export default Statistics;

export const Param = Option;