import React from 'react';
import { CircularProgress, Avatar, makeStyles } from '@material-ui/core';
import { Person } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    circular: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 1,
        width: '12vh !important',
        height: '12vh !important'
    },
    avatar: {
        width: '12vh',
        height: '12vh',
        '& > svg': {
            width: '8vh',
            height: '8vh',
        }
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    }
}));

function CircularAvatar({ loading }) {
    const classes = useStyles();

    return (
        <div className={`${classes.wrapper}`}>
            <Avatar className={`${classes.avatar}`}>
                <Person />
            </Avatar>
            {loading && <CircularProgress className={`${classes.circular}`} />}
        </div>
    );
}

export default CircularAvatar;