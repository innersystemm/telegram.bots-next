import React from 'react';
import {Link} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';

const styles = makeStyles({
    footer: props => ({
        marginTop: 'auto',
    }),
    toolbar: {
        background: '#181a1b',
    },
    footerContent: {
        columnCount: 3,
        textAlign: 'center',
        listStyleType: 'none',
        fontSize: 16,
        fontFamily: 'Open Sans, sans-serif',
        fontWeight: 300,
    },
    footerItemWrapper: {
        textDecoration: 'none',
    },
    footerItem: {
        color: 'white',
        '&: hover': {
            color: '#e83535'
        }
    }
});

function Footer({ children, columns}) {
    const classes = styles();
    return (
        <footer className={`${classes.footer} ${classes.toolbar}`}>
            <ul className={classes.footerContent}>
                <li><Link className={classes.footerItem} href="/users">Пользователи</Link></li>
                <li><Link className={classes.footerItem} href="/orders">Заказы</Link></li>
                <li><Link className={classes.footerItem} href="/news">Новости</Link></li>
                <li><Link className={classes.footerItem} href="#">Telegram-бот</Link></li>
            </ul>
        </footer>
    )
}

export default Footer;