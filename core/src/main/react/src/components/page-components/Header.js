import React from 'react';
import {AppBar, Avatar, Chip, Grid, Toolbar, Tooltip, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FaceRounded} from '@material-ui/icons';

const useStyles = makeStyles({
    header: {
        background: '#181a1b',
        width: '100%'
    },
    chip: {
        background: '#ce3b3b',
        '&:hover': {
            background: '#e83535'
        },
        '&:focus': {
            background: '#e83535'
        }
    },
    avatar: {
        background: '#9e2020',
        color: 'white'
    }
})

function Header({ title = 'Admin panel' }) {
    const classes = useStyles();
    return (
        <AppBar position="sticky" className={classes.header}>
            <Toolbar>
                <Grid container direction="row" justify="space-between">
                    <Grid item>
                        <Typography variant="h5">{title}</Typography>
                    </Grid>
                    <Grid item>
                        <Tooltip title="Нажмите чтобы открыть меню">
                            <Chip
                                avatar={
                                    <Avatar className={classes.avatar}>
                                        <FaceRounded />
                                    </Avatar>
                                }
                                label="Clickable Deletable Chip"
                                className={classes.chip}
                                clickable
                            />
                        </Tooltip>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}

export default Header;