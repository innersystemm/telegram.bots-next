import React, {useState} from 'react';
import {Grid, Hidden} from '@material-ui/core';
import {connect} from 'react-redux';
import {makeStyles} from '@material-ui/styles';
import Header from './Header';
import Statistics, {Param} from '../Statistics';
import LeftPaneMenuList from './LeftPaneMenuList';
import Background from '../../background.jpg';

const styles = makeStyles(theme => ({
    item: ({ margin = 2 }) => ({
        paddingTop: `${margin}vh`,
    }),
    statistics: ({ margin = 2 }) => ({
        paddingTop: `${margin}vh !importnant`,
    }),
    container: {
        flex: '1 0 auto',
        padding: 0,
        backgroundImage: `url(${Background})`,
        backgroundAttachment: 'fixed',
        backgroundPosition: 'center left',
        backgroundRepeat: ' no-repeat',
        backgroundSize: 'cover',
        transition: 'all 0.3s ease'
    }
}));

function Page({ children, className, usersReducer: { users, usersTotal } }) {
    const [title, setTitle] = useState();

    const classes = styles();
    return (
        <div className={`${classes.container} ${className}`}>
            <Header title={title} />
            <Grid container justify="space-evenly" className={classes.root}>
                <Grid container item direction="column" xs={1} md={2} lg={2}>
                    <Grid item className={classes.item}>
                        <LeftPaneMenuList square onMenuClick={name => setTitle(name)} />
                    </Grid>
                    <Grid item className={classes.item}>
                        <Hidden xsDown smDown>
                            <Statistics square title="Новости">
                                <Param name="Всего новостей" value="4" />
                                <Param name="Подборок новостей" value="4" />
                            </Statistics>
                        </Hidden>
                    </Grid>
                    <Grid item className={classes.item}>
                        <Hidden xsDown smDown>
                            <Statistics square title="Пользователи">
                                <Param name="Пользователей всего" value={usersTotal} />
                                <Param name="Пользователей за последние 24 часа" value="4" />
                            </Statistics>
                        </Hidden>
                    </Grid>
                </Grid>
                <Grid item xs={8} md={7} lg={8} xl={9} className={classes.item}>
                    {children}
                </Grid>
            </Grid>
        </div>
    )
}

export default connect(state => state)(Page);