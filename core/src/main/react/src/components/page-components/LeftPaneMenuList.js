import {Hidden, ListItemIcon, MenuItem, MenuList, Paper, Tooltip, Typography} from '@material-ui/core';
import {AccountCircle, ImportContacts, Reorder} from '@material-ui/icons';
import {makeStyles} from '@material-ui/styles';
import React, {useState} from 'react';
import {withRouter} from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {

    }
}));

function LeftPaneMenuList({ history, onMenuClick, square }) {
    const [index, setIndex] = useState(-1);

    const classes = useStyles();

    const handleMenuClick = title => {
        onMenuClick && onMenuClick(title);
    };

    return (
        <Paper square={square}>
            <MenuList >
                <Tooltip title="Открыть список пользователей">
                    <MenuItem onClick={() => { history.push("/users"); handleMenuClick('Пользователи') }} >
                        <ListItemIcon>
                            <AccountCircle />
                        </ListItemIcon>
                        <Hidden xsDown smDown><Typography>Пользователи</Typography></Hidden>
                    </MenuItem>
                </Tooltip>
                <Tooltip title="Открыть список активных заказов">
                    <MenuItem onClick={() => { history.push("/order"); handleMenuClick('Заказы') }}>
                        <ListItemIcon>
                            <Reorder />
                        </ListItemIcon>
                        <Hidden xsDown smDown><Typography>Заказы</Typography></Hidden>
                    </MenuItem>
                </Tooltip>
                <Tooltip title="Перейти к управлению новостями">
                    <MenuItem onClick={() => { history.push("/news"); handleMenuClick('Новости') }}>
                        <ListItemIcon>
                            <ImportContacts />
                        </ListItemIcon>
                        <Hidden xsDown smDown><Typography>Новости</Typography></Hidden>
                    </MenuItem>
                </Tooltip>
            </MenuList>
        </Paper >
    )
};

export default withRouter(LeftPaneMenuList);

