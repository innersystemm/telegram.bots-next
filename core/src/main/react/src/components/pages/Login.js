import React, { useState } from 'react';
import { Paper, Grid, TextField, makeStyles, Button, createMuiTheme, MuiThemeProvider, Avatar, Checkbox, FormControlLabel, Typography, CircularProgress } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import { Person } from '@material-ui/icons';
import { connect } from 'react-redux';
import { processLogin } from '../../redux/auth/auth';
import CircularAvatar from '../elements/CircularAvatar';

export const themePrimary = createMuiTheme({
    palette: {
        primary: red,
    },
});

const useStyles = makeStyles(theme => ({
    '@keyframes opacity': {
        from: {
            opacity: 0
        },
        to: {
            opacity: 1
        }
    },
    root: {
        display: 'flex',
        marginTop: 'auto',
        justifyContent: 'center',
        animationName: '$opacity',
        animationDuration: '.5s'
    },
    paper: {
        margin: theme.spacing(3),
        padding: theme.spacing(2),
    },
    avatar: {
        width: '12vh',
        height: '12vh',
        '& > svg': {
            width: '8vh',
            height: '8vh',
        }
    },
    rememberMe: {
        fontSize: 13,
        color: 'rgba(0, 0, 0, 0.54)'
    },
    noPadding: {
        paddingTop: '0 !important',
        paddingBottom: '0 !important'
    },
    typography: {
        fontSize: 10,
        textAlign: 'center',
        animationName: '$error',
        animationDuration: '1s'
    },
    '@keyframes error': {
        from: {
            opacity: 0
        },
        to: {
            opacity: 1
        }
    }
}));

function LoginPage({ minUsernameLength = 4, minPasswordLength = 6, dispatch, authReducer }) {
    const classes = useStyles();

    const [username, setUsername] = useState('');
    const [usernameError, setUsernameError] = useState(false);

    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);

    const [rememberMe, setRememberMe] = useState(false);

    const validateUsername = (username) => {
        if (!username || username.length < minUsernameLength) {
            setUsernameError(true);
            return;
        }
        setUsernameError(false);
    };

    const validatePassword = (password) => {
        if (!password || password.length < minPasswordLength) {
            setPasswordError(true);
            return;
        }
        setPasswordError(false);
    };

    const login = () => {
        validateUsername();
        validatePassword();
        if (usernameError || passwordError) {
            return;
        }
        console.log('dispathc', dispatch, 'reducer', authReducer)
        dispatch(processLogin({ username, password }));
    }

    return (
        <MuiThemeProvider theme={themePrimary}>
            <div className={classes.root}>
                <Paper square className={classes.paper}>
                    <Grid container spacing={3} direction="column" justify="center">
                        <Grid item container justify="center">
                            <CircularAvatar className={classes.avatar}/>
                        </Grid>
                        <Grid item>
                            <TextField
                                error={usernameError}
                                value={username}
                                margin="dense"
                                variant="outlined"
                                onInput={e => setUsername(e.target.value)}
                                onBlur={e => validateUsername(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                label="Логин"
                            />
                            {usernameError && <Typography color="error" className={classes.typography}>Минимальная длина - {minUsernameLength} символов</Typography>}
                        </Grid>
                        <Grid item>
                            <TextField
                                error={passwordError}
                                value={password}
                                type="password"
                                margin="dense"
                                variant="outlined"
                                onChange={e => setPassword(e.target.value)}
                                onBlur={e => validatePassword(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                label="Пароль"
                            />
                            {passwordError && <Typography color="error" className={classes.typography}>Минимальная длина - {minPasswordLength} символов</Typography>}
                        </Grid>
                        <Grid item className={classes.noPadding}>
                            <FormControlLabel
                                classes={{
                                    label: classes.rememberMe
                                }}
                                control={
                                    <Checkbox checked={rememberMe} onChange={() => setRememberMe(!rememberMe)} color="primary" />
                                }
                                label="Запомнить меня"
                            />
                        </Grid>
                        <Grid item>
                            <Button onClick={login} variant="contained" color="primary">Войти</Button>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        </MuiThemeProvider>
    )
}

export default connect(state => state)(LoginPage);