import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {loadUsers} from '../../redux/users/users';
import {Container, makeStyles, Paper} from '@material-ui/core';
import MaterialTable from '../Table/MaterialTable';

const columns = [{
    name: 'Name',
    dataField: 'firstName',
    sort: true
}, {
    name: 'Second Name',
    dataField: 'secondName',
    sort: true
}]

const useStyles = makeStyles(theme => ({
    root: {

    }
}));

function UsersList({ className, dispatch, usersReducer: { users, usersTotal } }) {
    const [page, setPage] = useState(0);
    const [size, setSize] = useState(10);
    const [sort, setSort] = useState('');

    const classes = useStyles();

    useEffect(() => {
        dispatch(loadUsers({ page, size, sort }));
    }, []);

    return (
        <Container>
            <Paper square className={`${classes.root} ${className}`}>
                <MaterialTable
                    selectableRows
                    small
                    columns={columns}
                    data={{ data: users, total: usersTotal }}
                    events={{
                        onChangePage: page => setPage(page),
                        onChangeRowsPerPage: rowsPerPage => setSize(rowsPerPage)
                    }} />
            </Paper>
        </Container>
    );
};

export default connect(state => state)(UsersList);