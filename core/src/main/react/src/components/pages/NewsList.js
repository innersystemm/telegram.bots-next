import React, {useEffect, useState} from 'react';
import MaterialTable from '../Table/MaterialTable';
import {connect} from 'react-redux';
import {Container, Paper} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {addNews, deleteNews, loadNews} from '../../redux/news/news';

const columns = [{
    name: 'Содержание',
    dataField: 'content',
    sort: false
}, {
    name: 'Обновлен',
    dataField: 'fetchDate',
    sort: true,
    type: 'date'
}];

const useStyles = makeStyles({
    root: {
        padding: '1vh'
    }
});

function NewsList({ dispatch, newsReducer }) {
    const [page, setPage] = useState(0);
    const [size, setSize] = useState(10);
    const [sort, setSort] = useState('');

    const classes = useStyles();

    useEffect(() => {
        dispatch(loadNews({ page, size, sort }));
    }, []);

    return (
        <Container>
            <Paper square className={classes.root}>
                <MaterialTable
                    selectableRows
                    small
                    columns={columns}
                    data={{ data: newsReducer.news, total: newsReducer.newsTotal }}
                    events={{
                        onItemAdded: data => dispatch(addNews(data)),
                        onItemChanged: data => dispatch(deleteNews(data))
                    }} />
            </Paper>
        </Container >
    )
}

export default connect(state => state)(NewsList);