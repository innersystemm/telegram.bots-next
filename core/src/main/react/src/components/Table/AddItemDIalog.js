import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import {Button, ButtonGroup, Dialog, DialogTitle, Grid, Paper, TextField, Tooltip} from '@material-ui/core';
import {Close, Save} from '@material-ui/icons';

const useStyles = makeStyles({
    addRecordText: {
        margin: '1vh',
        '&:placeholder': {
            fontSize: 13,
            fontWeight: 300,
            color: 'red'
        }
    }
});

function AddItemDialog({ show, columns = [], onDataInsertionCompleted, onClose }) {
    const [data, setData] = useState({});

    const classes = useStyles();

    return (
        <Dialog open={show} onClose={onClose}>
            <DialogTitle key="title">
                Добавить запись
            </DialogTitle>
            <Paper key="menu">
                <Grid container direction="row" wrap="wrap">
                    {columns.map((column, index) =>
                        <Grid item key={index} xs={6}>
                            <TextField
                                variant="outlined"
                                {...column.type ? { type: column.type } : {}}
                                label={column.name}
                                className={classes.addRecordText}
                                InputLabelProps={{ shrink: true }}
                                margin="dense"
                                onChange={e => setData({ ...data, [column.dataField]: e.target.value })}
                            />
                        </Grid>
                    )}
                </Grid>
                <ButtonGroup>
                    <Button onClick={() => { onDataInsertionCompleted(data); onClose(); }}>
                        <Tooltip title="Сохранить">
                            <Save />
                        </Tooltip>
                    </Button>
                    <Button onClick={onClose}>
                        <Tooltip title="Закрыть">
                            <Close />
                        </Tooltip>
                    </Button>
                </ButtonGroup>
            </Paper>
        </Dialog>
    )
}

export default AddItemDialog;

AddItemDialog.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        dataField: PropTypes.string.isRequired,
        sort: PropTypes.bool,
        type: PropTypes.oneOf(['date', 'text', 'numeric'])
    }).isRequired),
};