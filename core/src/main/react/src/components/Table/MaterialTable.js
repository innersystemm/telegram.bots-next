import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    ButtonGroup,
    Checkbox,
    Grid,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
    TextField,
    Tooltip
} from '@material-ui/core';
import {Add, Close} from '@material-ui/icons';
import TablePaginationActions from '@material-ui/core/TablePagination/TablePaginationActions';
import AddItemDialog from './AddItemDIalog';
import {red} from '@material-ui/core/colors';

const EMPTY_FUNC = () => { };

const useStyles = makeStyles({
    paginator: {
        borderBottom: 'none'
    },
    toolbar: {
        padding: '1vh',
        paddingLeft: '2vw',
        paddingRight: '2vw',
        borderBottom: 'solid 1px rgba(224, 224, 224, 1)'
    },
    icon: {
        size: 24,
        cursor: 'pointer'
    },
    searchField: {
        margin: 0
    },
    noData: {
        textAlign: 'center'
    },
    tootlipButton: {

    },
    checkbox: {
        color: red[400],
        '&:checked': {
            display: 'none',
            color: red[200],
        },
    }
});

function MaterialTable({ selectableRows, columns = [], className, data: { data = [], total = 0 }, events = {}, small, styles, rowsPerPageOptions = [5, 10, 15, 25] }) {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const [showModal, setShowModal] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]);

    const {
        onChangePage = EMPTY_FUNC,
        onChangeRowsPerPage = EMPTY_FUNC,
        onItemAdded = EMPTY_FUNC,
        onItemChanged = EMPTY_FUNC
    } = events;

    let classes = useStyles();

    if (styles) {
        classes = makeStyles(styles)();
    };

    useEffect(() => {
        console.log(selectedRows);
    }, [selectedRows]);

    const handleChangePage = e => {
        setPage(e.target.value);
        onChangePage(e.target.value);
    };

    const handleChangeRowsPerPage = e => {
        setRowsPerPage(e.target.value);
        onChangeRowsPerPage(e.target.value);
    };

    const handleDataInsertionCompleted = data => {
        onItemAdded(data);
    };

    const handleSelectedColumns = (e, index) => {
        const selectedColumnIndex = selectedRows.findIndex(({rowIndex})=> rowIndex === index);
        console.log(selectedColumnIndex)
        if (selectedColumnIndex >= 0) {
            setSelectedRows([...selectedRows.filter((value, rowIndex) => rowIndex !== selectedColumnIndex)]);
            return;
        }
        setSelectedRows([...selectedRows, { rowIndex: index }]);
    };

    const handleItemRemove = () => {
        const items = [];
        selectedRows.forEach(item => items.push(data[item.index]));
        onItemChanged(items);
    };

    return (
        <div className={className}>
            <AddItemDialog
                columns={columns}
                show={showModal}
                onClose={() => setShowModal(!showModal)}
                onDataInsertionCompleted={handleDataInsertionCompleted} />
            <div className={classes.toolbar}>
                <Grid container direction="row" alignItems="center" justify="space-between">
                    <Grid item>
                        <ButtonGroup>
                            <Tooltip title="Добавить новую запись">
                                <Button onClick={() => setShowModal(true)} className={classes.tootlipButton}>
                                    <Add className={classes.icon} />
                                </Button>
                            </Tooltip>
                            <Tooltip title="Удалить выбранные">
                                <Button onClick={handleItemRemove} className={classes.tootlipButton}>
                                    <Close className={`${classes.icon}`} />
                                </Button>
                            </Tooltip>
                        </ButtonGroup>
                    </Grid>
                    <Grid item>
                        <TextField className={classes.searchField} variant="outlined" margin="dense" label="Поиск" />
                    </Grid>
                </Grid>
            </div>
            <Table>
                <TableHead>
                    <TableRow>
                        {selectableRows && <TableCell {...small ? { padding: 'checkbox' } : {}}><Checkbox color="default" className={classes.checkbox} /></TableCell>}
                        {columns.map((column, index) => <TableCell key={index}>{<TableSortLabel disabled={!column.sort}>{column.name}</TableSortLabel>}</TableCell>)}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, index) =>
                        <TableRow key={index}>
                            {selectableRows &&
                                <TableCell {...small ? { padding: 'checkbox' } : {}}>
                                    <Checkbox className={classes.checkbox} onClick={e => handleSelectedColumns(e, index)} />
                                </TableCell>}
                            {columns.map((column, cindex) => <TableCell {...small ? { size: 'small' } : {}} key={cindex} >{row[column.dataField]}</TableCell>)}
                        </TableRow>
                    )}
                    {!data.length && <TableRow className={classes.noData}><TableCell colSpan={columns.length}>Нет данных</TableCell></TableRow>}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            className={classes.paginator}
                            component={TableCell}
                            {...small ? { padding: 'none' } : {}}
                            rowsPerPageOptions={rowsPerPageOptions}
                            colSpan={columns.size}
                            count={total}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'Записей на страницу' },
                                native: false,
                            }}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </div>
    );
}



MaterialTable.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        dataField: PropTypes.string.isRequired,
        sort: PropTypes.bool,
        type: PropTypes.oneOf(['date', 'text', 'numeric'])
    }).isRequired),
    data: PropTypes.object.isRequired,
    events: PropTypes.shape({
        onChangePage: PropTypes.func,
        onChangeRowsPerPage: PropTypes.func,
        onItemAdded: PropTypes.func,
        onItemChanged: PropTypes.func
    }),
    className: PropTypes.string
}

export default MaterialTable;