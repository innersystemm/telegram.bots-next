export default class UsersApi {
    static fetchUsers({ page, size, sort }) {
        const url = new URL(`${window.location.origin}/api/customers`);
        url.searchParams.append('page', page);
        url.searchParams.append('size', size);
        url.searchParams.append('sort', sort);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url).then(res => res.json());
    }
}