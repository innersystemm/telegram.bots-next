export default class NewsApi {
    static fetchNews({ page, size, sort }) {
        const url = new URL(`${window.location.origin}/api/news`);
        url.searchParams.append('page', page);
        url.searchParams.append('size', size);
        url.searchParams.append('sort', sort);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url).then(res => res.json());
    }

    static addNews(data) {
        const url = new URL(`${window.location.origin}/api/news`);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url, {
            method: 'PUT',
            headers: { 'Access-Control-Allow-Origin': '*' },
            body: JSON.stringify(data)
        }).then(res => res.json());
    }

    static deleteNews(data) {
        const url = new URL(`${window.location.origin}/api/news`);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url, {
            method: 'DELETE',
            body: JSON.stringify(data)
        }).then(res => res.json());
    }
}