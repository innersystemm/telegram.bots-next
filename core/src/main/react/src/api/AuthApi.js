export default class AuthApi {
    static login(credentials) {
        const url = new URL(`${window.location.origin}/login`);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url, {
            method: 'POST',
            headers: { 'Access-Control-Allow-Origin': '*' },
            body: JSON.stringify(credentials)
        }).then(res => res.json());
    }

    static register(credentials) {
        const url = new URL(`${window.location.origin}/register`);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url, {
            method: 'POST',
            headers: { 'Access-Control-Allow-Origin': '*' },
            body: JSON.stringify(credentials)
        }).then(res => res.json());
    }

    static logout(credentials) {
        const url = new URL(`${window.location.origin}/logout`);
        if (url.port === '3000') {
            url.port = 8080;
        }
        return fetch(url, {
            method: 'POST',
            headers: { 'Access-Control-Allow-Origin': '*' },
            body: JSON.stringify(credentials)
        }).then(res => res.json());
    }
}